+++
title = "About"
date = "2020-05-11"
+++

Hello world, this is Yuri but you can call me Rick.

I'm a passionate Software Engineer who lives in Cuba and currently works at the University of Camagüey building software in a small and agile team, 
at the same time I've had teach Data Structures, and derivatives topics from Geographic Information Systems and Linux Server Administration.
I also worked as a Web Developer at CubaMóvil+.

If you want to get in touch, throw me a few words in [Telegram](https://t.me/rickmclean).
